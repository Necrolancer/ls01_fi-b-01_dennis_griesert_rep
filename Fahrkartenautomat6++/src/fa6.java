import java.security.AlgorithmConstraints;
import java.util.Scanner;

class fa6 {
	
	static Scanner tastatur = new Scanner(System.in);
	static double zuZahlenderBetrag;
	static double ticketpreis = 0;
	public static void main(String[] args) {
		
		double tickets;
		tickets = fahrkartenbestellungErfassen();
		
		double eingezahlterGesamtbetrag;
		eingezahlterGesamtbetrag = fahrkartenBezahlen();
		
		fahrkartenAusgeben();
	
		rueckgeldAusgeben(eingezahlterGesamtbetrag);
	}
	// 	 W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:
	// 	 Einzelfahrschein Regeltarif AB [2,90 EUR] (1)
	//	 Tageskarte Regeltarif AB [8,60 EUR] (2)
	//	 Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)
		  
	public static double fahrkartenbestellungErfassen() {
		double tickets = 0;
		int ticketart = 0;
		
		while (ticketart != 9){
		System.out.println("Fahrkartenbestellvorgang: ");
		System.out.println("========================= ");
		System.out.println();
		System.out.println("W�hlen Sie: ");
		System.out.println("  Einzelfahrscheine (1) ");
		System.out.println("  Tageskarten (2) ");
		System.out.println("  Gruppenkarten (3) ");
		System.out.println("  Bezahlen (9) ");
		
		ticketart = (int)tastatur.nextDouble();		// Ticketart
		System.out.println("Ihre Wahl: " + ticketart);
		System.out.println();
		while (ticketart < 1 || ticketart > 3 && ticketart != 9 )
		{
			System.out.println("Auswahl der Ticketart ist nicht vorhanden. Bitte nochmal versuchen.");
			System.out.println("Ticketart: ");
			
			ticketart = tastatur.nextInt();
		};
		
		
	
		if (ticketart != 9){
		
			System.out.println();
		System.out.println("Wie viele Tickets: ");
		System.out.println();
		tickets = tastatur.nextDouble();
		while (tickets <= 0 || tickets > 10)
		{
			System.out.println("Falscher Eingabewert: Anzahl der Tickets darf nicht <=0 oder >10 sein!");
			System.out.println("Wie viele Tickets: ");
			System.out.println();
			tickets = tastatur.nextDouble();
		};
		}
		
		switch(ticketart){
        case 1:
            System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] * " + tickets + " Tickets");
            ticketpreis += (2.90 * tickets);
            System.out.println("Zwischenbetrag: " + ticketpreis + "0 Euro");
            break;
        case 2:
            System.out.println("Tageskarte Regeltarif AB [8,60 EUR] * " + tickets + " Tickets");
            ticketpreis += (8.60 * tickets);
            System.out.println("Zwischenbetrag: " + ticketpreis + "0 Euro");
            break;
        case 3:
            System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] * " + tickets + " Tickets");
            ticketpreis += (23.50 * tickets);
            System.out.println("Zwischenbetrag: " + ticketpreis + "0 Euro");
            break;
        case 9:
        	System.out.println("Zu zahlender Gesamtbetrag: " + ticketpreis + "0 Euro");
        
            
            break; 
		}}
            return ticketpreis;}

	public static double fahrkartenBezahlen() {
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < ticketpreis) {
		//	System.out.println(eingezahlterGesamtbetrag);
		//	System.out.println(">> " + eingezahlterGesamtbetrag + "< ( " + zuZahlenderBetrag + ")");

		System.out.printf("Noch zu zahlen: %.2f EURO \n", ((ticketpreis) - eingezahlterGesamtbetrag));
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		eingeworfeneM�nze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static double muenzeAusgeben(double r�ckgabebetrag, double betrag, String einheit) 
	{
		
		if(einheit == "Cent") {
			System.out.println((int)betrag + " " + einheit);
			betrag = betrag / 100;
			}
		else {
			System.out.println(betrag + " " + einheit);
		}
		return r�ckgabebetrag -= betrag;
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag;
		
		r�ckgabebetrag = (eingezahlterGesamtbetrag - ticketpreis);

		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,2,"Euro");
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,1,"Euro");
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,50,"Cent");
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,20,"Cent");
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,10,"Cent");
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,5,"Cent");
			}

		}
	}

}