import java.security.AlgorithmConstraints;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

class fa7 {
	static List<String[]> karten = new ArrayList<String[]>();
	
//1 	Einzelfahrschein Berlin AB 	2,90
//2 	Einzelfahrschein Berlin BC 	3,30
//3 	Einzelfahrschein Berlin ABC 	3,60
//4 	Kurzstrecke 	1,90
//5 	Tageskarte Berlin AB 	8,60
//6 	Tageskarte Berlin BC 	9,00
//7 	Tageskarte Berlin ABC 	9,60
//8 	Kleingruppen-Tageskarte Berlin AB 	23,50
//9 	Kleingruppen-Tageskarte Berlin BC 	24,30
//10 	Kleingruppen-Tageskarte Berlin ABC 	24,90
	
	
	static Scanner tastatur = new Scanner(System.in);
	static double zuZahlenderBetrag;
	static double ticketpreis = 0;
	static DecimalFormat df = new DecimalFormat("#.00");

	
	public static void main(String[] args) {
		
		String[] karte = new String[2];
		karte[0] = "Einzelfahrschein Berlin AB";
		karte[1] = "2.90";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Einzelfahrschein Berlin BC";
		karte[1] = "3.30";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Einzelfahrschein Berlin ABC";
		karte[1] = "3.60";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Kurzstrecke";
		karte[1] = "1.90";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Tageskarte Berlin AB";
		karte[1] = "8.60";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Tageskarte Berlin BC";
		karte[1] = "9.00";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Tageskarte Berlin ABC";
		karte[1] = "9.60";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Kleingruppen-Tageskarte Berlin AB";
		karte[1] = "23.50";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Kleingruppen-Tageskarte Berlin BC";
		karte[1] = "24.30";
		karten.add(karte);
		karte = new String[2];
		karte[0] = "Kleingruppen-Tageskarte Berlin ABC";
		karte[1] = "24.90";
		karten.add(karte);
		
		double tickets;
		tickets = fahrkartenbestellungErfassen();
		
		double eingezahlterGesamtbetrag;
		eingezahlterGesamtbetrag = fahrkartenBezahlen();
		
		fahrkartenAusgeben();
	
		rueckgeldAusgeben(eingezahlterGesamtbetrag);
	}

		  
	public static double fahrkartenbestellungErfassen() {
		int tickets = 0;
		int ticketart = 0;
		
		while (ticketart != 10){
		System.out.println("Fahrkartenbestellvorgang: ");
		System.out.println("========================= ");
		System.out.println();
	
		System.out.println("W�hlen Sie: ");
		for (int i = 0; i < karten.size(); i++) {
			System.out.println("  " + karten.get(i)[0] + " [" + karten.get(i)[1] + "] EURO (" + i + ")");
		}
		//System.out.println("  Einzelfahrscheine (1) ");
		//System.out.println("  Tageskarten (2) ");
		//System.out.println("  Gruppenkarten (3) ");
		System.out.println("  Bezahlen (10) ");
		
		ticketart = (int)tastatur.nextDouble();		// Ticketart
		System.out.println("Ihre Wahl: " + ticketart);
		System.out.println();
		while (ticketart < 0 && ticketart != 10 )
		{
			System.out.println("Auswahl der Ticketart ist nicht vorhanden. Bitte nochmal versuchen.");
			System.out.println("Ticketart: ");
			
			ticketart = tastatur.nextInt();
		};
		
		
	
		if (ticketart != 10)
		{
		
			System.out.println();
			System.out.println("Wie viele Tickets: ");
			System.out.println();
			tickets = tastatur.nextInt();
			while (tickets <= 0 || tickets > 10)
			{
				System.out.println("Falscher Eingabewert: Anzahl der Tickets darf nicht <=0 oder >10 sein!");
				System.out.println("Wie viele Tickets: ");
				System.out.println();
				tickets = tastatur.nextInt();
			};
		}
		if (ticketart == 10)
		{
			System.out.println("Zu zahlender Gesamtbetrag: " + df.format(ticketpreis) + " Euro");
		}else {
			System.out.println(karten.get(ticketart)[0] + " [" + karten.get(ticketart)[1] + "] " + tickets + " Tickets");
            ticketpreis += ( Double.parseDouble(karten.get(ticketart)[1]) * tickets);
            System.out.println("Zwischenbetrag: " + df.format(ticketpreis) + " Euro");
		}
		} 
		
		
            return ticketpreis;}

	public static double fahrkartenBezahlen() {
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < ticketpreis) {
		//	System.out.println(eingezahlterGesamtbetrag);
		//	System.out.println(">> " + eingezahlterGesamtbetrag + "< ( " + zuZahlenderBetrag + ")");

		System.out.printf("Noch zu zahlen: %.2f EURO \n", ((ticketpreis) - eingezahlterGesamtbetrag));
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		eingeworfeneM�nze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static double muenzeAusgeben(double r�ckgabebetrag, double betrag, String einheit) 
	{
		
		if(einheit == "Cent") {
			System.out.println((int)betrag + " " + einheit);
			betrag = betrag / 100;
			}
		else {
			System.out.println(betrag + " " + einheit);
		}
		return r�ckgabebetrag -= betrag;
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag;
		
		r�ckgabebetrag = (eingezahlterGesamtbetrag - ticketpreis);

		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,2,"Euro");
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,1,"Euro");
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,50,"Cent");
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,20,"Cent");
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,10,"Cent");
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,5,"Cent");
			}

		}
	}

}