import java.security.AlgorithmConstraints;
import java.util.Scanner;

class fa4 {
	
	static Scanner tastatur = new Scanner(System.in);
	static double zuZahlenderBetrag;
	
	public static void main(String[] args) {
		
		double tickets;
		tickets = fahrkartenbestellungErfassen();
		
		double eingezahlterGesamtbetrag;
eingezahlterGesamtbetrag = fahrkartenBezahlen(tickets);
		
		fahrkartenAusgeben();
	
		rueckgeldAusgeben(eingezahlterGesamtbetrag,tickets);
	}

	public static double fahrkartenbestellungErfassen() {

		double tickets;
		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		System.out.print("Wie viele Tickets: ");
		tickets = tastatur.nextDouble();
		return tickets;
	}

	public static double fahrkartenBezahlen(double tickets) {
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < (zuZahlenderBetrag * tickets)) {
		//	System.out.println(eingezahlterGesamtbetrag);
		//	System.out.println(">> " + eingezahlterGesamtbetrag + "< ( " + zuZahlenderBetrag + ")");

		System.out.printf("Noch zu zahlen: %.2f EURO \n", ((zuZahlenderBetrag * tickets) - eingezahlterGesamtbetrag));
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		eingeworfeneM�nze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static double muenzeAusgeben(double r�ckgabebetrag, double betrag, String einheit) 
	{
		
		if(einheit == "Cent") {
			System.out.println((int)betrag + " " + einheit);
			betrag = betrag / 100;
			}
		else {
			System.out.println(betrag + " " + einheit);
		}
		return r�ckgabebetrag -= betrag;
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double tickets) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag;
		
		r�ckgabebetrag = (eingezahlterGesamtbetrag - (zuZahlenderBetrag * tickets));

		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,2,"Euro");
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,1,"Euro");
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,50,"Cent");
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,20,"Cent");
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,10,"Cent");
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				r�ckgabebetrag = muenzeAusgeben(r�ckgabebetrag,5,"Cent");
			}

		}
	}

}